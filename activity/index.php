<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Array - s05 Activity</title>
</head>
<body>

    <h1>Divisible of Five</h1>
    <p><?= printDivisibleOfFive() ?></p>
    
    <hr>

    <h1>Array Manipulation</h1>
    <?php array_push($students, 'John Smith') ?></p>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students) ?></p>

    <?php array_push($students, 'Jane Smith') ?></p>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students) ?></p>

    <?php array_shift($students) ?></p>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students) ?></p>

    <hr>


   



</body>
</html>